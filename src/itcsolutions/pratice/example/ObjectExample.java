package itcsolutions.pratice.example;

/**
 * Пример класса по которому создается объект
 *
 * @author mikhailyuk
 * @since 29.06.2021
 */
public class ObjectExample {

    private String exampleStringValue = "Hello, kotyatki";

    /**
     * Метод для примера номер 1 - метод, просто возвращающий значение внутренней переменной
     * @return
     */
    public String getExampleStringValue() {
        return exampleStringValue;
    }

    /**
     * Метод для примера номер 2 - метод, который выводит переданное ему значение переменной value в консоль
     * @param someValue - некая переменная с типом String (строка)
     */
    public void exampleMethod2(String someValue) {
        System.out.println(someValue);
    }
}
